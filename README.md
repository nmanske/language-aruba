# Aruba Language Schema

Aruba language syntax for Atom editor. Tested on the following devices:

* Aruba 2xx/3xx IAPs
* 25xx/29xx switches
* 7xxx controllers

## Installation

cd into your .atom/packages directory and then run:

    git clone https://github.com/nmanske/language-aruba

## Packages

Recommended
* custom-folds
* highlight-selected
* open-recent
* minimap
* minimap-cursorline
* minimap-find-and-replace
* minimap-highlighted-selected

Optional
* pigments
* minimap-pigments
* color-picker
* split-diff
* minimap-split-diff
